package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.lang.Math;
import com.opencsv.CSVReader;

import API.IManager;
import model.data_structures.LinkedList;
import model.data_structures.ListIterator;
import model.data_structures.Queue;
import model.data_structures.SortStructure;
import model.data_structures.Stack;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;
import model.vo.Trip.Tipo;

public class Manager implements IManager {


	public final static String rutaGeneral="."+File.separator+"data"+File.separator;
	//Ruta del archivo de datos 2017-Q1
	// TODO Actualizar
	public static final String TRIPS_Q1 = rutaGeneral+"Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2
	// TODO Actualizar	
	public static final String TRIPS_Q2 = rutaGeneral+"Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3
	// TODO Actualizar	
	public static final String TRIPS_Q3 = rutaGeneral+"Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q4 = rutaGeneral+"Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de trips 2017-Q1-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q1_Q4 = "Ruta Trips 2017-Q1-Q4 en directorio data";

	//Ruta del archivo de stations 2017-Q1-Q2
	// TODO Actualizar	
	public static final String STATIONS_Q1_Q2 = rutaGeneral+"Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar	
	public static final String STATIONS_Q3_Q4 = rutaGeneral+"Divvy_Stations_2017_Q3Q4.csv";


	private Stack<Trip> stackTrips= new Stack<Trip>();

	private LinkedList<Bike> listaBikes = new LinkedList<Bike>();

	private LinkedList<Station> listaEncadenadaStations= new LinkedList<Station>();
	
	public Stack<Trip> getTrips()
	{
		return stackTrips;
	}
	
	public LinkedList<Bike> getBikes()
	{
		return listaBikes;
	}
	
	public LinkedList<Station> getStations()
	{
		return listaEncadenadaStations;
	}


	public Queue<Trip> A1ViajesEnPeriodoDeTiempo(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		LocalDateTime inicio = fechaInicial.minusNanos(1);
		LocalDateTime fin = fechaFinal.plusNanos(1);
		Stack<Trip> stack = stackTrips;
		Queue<Trip> r = new Queue<Trip>();
		ListIterator<Trip> listIte = (ListIterator<Trip>) stack.iterator();
		while(listIte.hasNext())
		{
			Trip actual = listIte.next();
			LocalDateTime inicioActual = actual.getStartTime();
			LocalDateTime finActual = actual.getStopTime();
			if(inicioActual.isAfter(inicio)&&finActual.isBefore(fin))
			{
				r.enqueue(actual);
			}
		}
		return r; //SortStructure.mergeSortTripQueue(r);

	}

	public LinkedList<Bike> A2BicicletasOrdenadasPorNumeroViajes(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Queue<Trip> trips = A1ViajesEnPeriodoDeTiempo(fechaInicial, fechaFinal);
		cargarBikes(trips);
		//listaBikes = SortStructure.mergeSortBikes(listaBikes);
		return listaBikes;

	}

	public LinkedList<Trip> A3ViajesPorBicicletaPeriodoTiempo(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {

		LocalDateTime inicio = fechaInicial.minusNanos(1);
		LocalDateTime fin = fechaFinal.plusNanos(1);
		Stack<Trip> stack = stackTrips;
		LinkedList<Trip> r = new LinkedList<Trip>();
		ListIterator<Trip> listIte = (ListIterator<Trip>) stack.iterator();
		while(listIte.hasNext())
		{
			Trip actual = listIte.next();
			LocalDateTime inicioActual = actual.getStartTime();
			LocalDateTime finActual = actual.getStopTime();
			int id = actual.getBikeId();
			if(id==bikeId && inicioActual.isAfter(inicio) && finActual.isBefore(fin))
			{
				r.add(actual);
			}
		}
		//r = SortStructure.mergeSortTripList(r);
		return r;

	}

	public LinkedList<Trip> A4ViajesPorEstacionFinal(int endStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {

		LocalDateTime inicio = fechaInicial.minusNanos(1);
		LocalDateTime fin = fechaFinal.plusNanos(1);
		Stack<Trip> stack = stackTrips;
		LinkedList<Trip> r = new LinkedList<Trip>();
		ListIterator<Trip> listIte = (ListIterator<Trip>) stack.iterator();
		while(listIte.hasNext())
		{
			Trip actual = listIte.next();
			LocalDateTime inicioActual = actual.getStartTime();
			LocalDateTime finActual = actual.getStopTime();
			int id = actual.getEndStationId();
			if(id==endStationId && inicioActual.isAfter(inicio) && finActual.isBefore(fin))
			{
				r.add(actual);
			}
		}
		//r = SortStructure.mergeSortTripList(r);
		return r;
	}

	public Queue<Station> B1EstacionesPorFechaInicioOperacion(LocalDateTime fechaComienzo) {
		LocalDateTime inicioGeneral = fechaComienzo.minusNanos(1);
		LinkedList<Station> listSt = listaEncadenadaStations;
		Queue<Station> queue = new Queue<Station>();
		ListIterator<Station> listIte = (ListIterator<Station>) listSt.iterator();
		while(listIte.hasNext()){
			Station actual = listIte.next();
			LocalDateTime inicioActual = actual.getStartDate();
			if(inicioActual.isAfter(inicioGeneral)){
				queue.enqueue(actual);
			}
		}
		return queue;
	}

	public LinkedList<Bike> B2BicicletasOrdenadasPorDistancia(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Queue<Trip> trips = A1ViajesEnPeriodoDeTiempo(fechaInicial, fechaFinal);
		cargarBikes(trips);
		//listaBikes = SortStructure.mergeSortBikesByDistances(listaBikes);
		return listaBikes;

	}

	public LinkedList<Trip> B3ViajesPorBicicletaDuracion(int bikeId, int tiempoMaximo, String genero) {
		Stack<Trip> stack = stackTrips;
		LinkedList<Trip> r = new LinkedList<Trip>();
		ListIterator<Trip> listIte = (ListIterator<Trip>) stack.iterator();
		while(listIte.hasNext())
		{
			Trip actual = listIte.next();
			int id = actual.getBikeId();
			if(id==bikeId && actual.getTripDuration()<tiempoMaximo && actual.getGender()==genero)
			{
				r.add(actual);
			}
		}

		return r; //SortStructure.mergeSortTripList(r);
	}

	public LinkedList<Trip> B4ViajesPorEstacionInicial(int startStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		LocalDateTime inicio = fechaInicial.minusNanos(1);
		LocalDateTime fin = fechaFinal.plusNanos(1);
		Stack<Trip> stack = stackTrips;
		LinkedList<Trip> r = new LinkedList<Trip>();
		ListIterator<Trip> listIte = (ListIterator<Trip>) stack.iterator();
		while(listIte.hasNext())
		{
			Trip actual = listIte.next();
			LocalDateTime inicioActual = actual.getStartTime();
			LocalDateTime finActual = actual.getStopTime();
			int id = actual.getStartStationId();
			if(id==startStationId && inicioActual.isAfter(inicio) && finActual.isBefore(fin))
			{
				r.add(actual);
			}
		}

		return r;//SortStructure.mergeSortTripList(r);

	}

	public void C1cargar(String rutaTrips, String rutaStations) {
		if(rutaStations.equals("")&&rutaTrips.equals(""))
		{
			stackTrips= null;

			listaBikes = null;

			listaEncadenadaStations= null;
		} 
		else {


			//cargar stations
			try{
				FileReader fr = new FileReader(new File(rutaStations));
				CSVReader br = new CSVReader(fr);

				String[] line = br.readNext();
				//Se avanza de nuevo porque la l�nea anterior no contiene datos
				line = br.readNext();

				while(line!=null)
				{
					String[] fecha = (line[6].replace(' ', '/').replace(':', '/')).split("/");
					int tamano = fecha.length;
					Integer[]fecha2 = new Integer[tamano];
					for(int i = 0; i< fecha2.length; i++)
					{
						fecha2[i] = Integer.parseInt(fecha[i]);
					}
					LocalDateTime ini = LocalDateTime.of(fecha2[2], fecha2[0], fecha2[1],fecha2[3],fecha2[4],tamano==6 ? fecha2[5]:00);
					Station s = new Station(Integer.parseInt(line[0]), line[1],ini, Double.parseDouble(line[3]),Double.parseDouble(line[4]));


					listaEncadenadaStations.add(s);

					line = br.readNext();
				}

				br.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}


			//cargar trips
			try{
				FileReader fr = new FileReader(new File(rutaTrips));
				CSVReader br = new CSVReader(fr);

				Trip t=null;


				String[] line = br.readNext();
				//Se avanza de nuevo porque la l�nea anterior no contiene datos
				line = br.readNext();
				while(line!=null)
				{


					String[] fechaInicio = (line[1].replace(' ', '/').replace(':', '/')).split("/");
					int tamano = fechaInicio.length;
					Integer[]fechaInicio2 = new Integer[tamano];
					String[] fechaFin = (line[2].replace(' ', '/').replace(':', '/')).split("/");
					Integer[]fechaFin2 = new Integer[tamano];

					for(int i = 0; i< fechaFin2.length; i++)
					{
						fechaInicio2[i] = Integer.parseInt(fechaInicio[i]);
						fechaFin2[i] = Integer.parseInt(fechaFin[i]);
					}

					LocalDateTime ini = LocalDateTime.of(fechaInicio2[2], fechaInicio2[0], fechaInicio2[1],fechaInicio2[3],fechaInicio2[4],(tamano==6 ? fechaInicio2[5]:00));
					LocalDateTime fini = LocalDateTime.of(fechaFin2[2], fechaFin2[0], fechaFin2[1],fechaFin2[3],fechaFin2[4],tamano==6 ? fechaFin2[5]:00);
					if(line[10]==null || line[10].equals(""))
					{
						t = new Trip(Integer.parseInt(line[0].replace("\"","")),  ini , fini , Integer.parseInt(line[3].replace("\"","")), 
								Integer.parseInt(line[4].replace("\"","")), Integer.parseInt(line[5].replace("\"","")), Integer.parseInt(line[7].replace("\"","")), Trip.UNKNOWN);

					}
					else{
						t = new Trip(Integer.parseInt(line[0].replace("\"","")),  ini , fini , Integer.parseInt(line[3].replace("\"","")), 
								Integer.parseInt(line[4].replace("\"","")), Integer.parseInt(line[5].replace("\"","")), Integer.parseInt(line[7].replace("\"","")), line[10]);

					}
					stackTrips.push(t);
					line = br.readNext();
				}
				br.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	public void cargarBikes(Queue<Trip> tQueue)
	{
		//SortStructure.mergeSortTripQueueById(tQueue);
		ListIterator<Trip> listIte = (ListIterator<Trip>) tQueue.iterator();
		Trip t = listIte.next();
		int distance = (int) calcularDistancia(t.getStartStationId(), t.getEndStationId());
		Bike bici = new Bike(t.getBikeId(),1,distance,t.getTripDuration());
		listaBikes.add(bici);

		while(listIte.hasNext()) {
			t = listIte.next();
			if(t.getBikeId()==bici.getBikeId()) {
				bici.addDistance(t.getDistance());
				bici.addTrips(1);
				bici.addDuration(t.getTripDuration());
			}

			else {
				bici = new Bike(t.getBikeId(),1,distance,t.getTripDuration());
				listaBikes.add(bici);
			}
		}
	}

	public void cargarBikes(Stack<Trip> tStack)
	{
		//SortStructure.mergeSortTripStackById(tStack);
		ListIterator<Trip> listIte = (ListIterator<Trip>) tStack.iterator();
		Trip t = listIte.next();
		int distance = (int) calcularDistancia(t.getStartStationId(), t.getEndStationId());
		Bike bici = new Bike(t.getBikeId(),1,distance,t.getTripDuration());
		listaBikes.add(bici);

		while(listIte.hasNext()) {
			t = listIte.next();
			if(t.getBikeId()==bici.getBikeId()) {
				bici.addDistance(t.getDistance());
				bici.addTrips(1);
				bici.addDuration(t.getTripDuration());
			}

			else {
				bici = new Bike(t.getBikeId(),1,distance,t.getTripDuration());
				listaBikes.add(bici);
			}
		}
		
	}

	public Queue<Trip> C2ViajesValidadosBicicleta(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		
		LocalDateTime inicio = fechaInicial.minusNanos(1);
		LocalDateTime fin = fechaFinal.plusNanos(1);
		Stack<Trip> stack = stackTrips;
		//SortStructure.mergeSortTripStack(stack);
		Stack<Trip> correctos = new Stack<Trip>();
		Queue<Trip> inconsistencia = new Queue<Trip>();
		for(int i = 0; i< stack.size(); i++)
		{
			Trip actual = stack.pop();
			LocalDateTime inicioActual = actual.getStartTime();
			LocalDateTime finActual = actual.getStopTime();
			int id = actual.getBikeId();
			if(id==bikeId && inicioActual.isAfter(inicio) && finActual.isBefore(fin))
			{
				if(correctos.isEmpty())
				{
					correctos.push(actual);
				}else
				{
					Trip viejo = correctos.pop();
					correctos.push(viejo);
					if(viejo.getEndStationId()==actual.getStartStationId())
					{
						correctos.push(actual);
					}else
					{
						Trip error = correctos.pop();
						inconsistencia.enqueue(error);inconsistencia.enqueue(actual);
						while (!correctos.isEmpty())
						{
							correctos.pop();
						}
						i--;
					}
				}
			}
		}
		
		return inconsistencia;
	}

	public LinkedList<Bike> C3BicicletasMasUsadas(int topBicicletas) {
		cargarBikes(stackTrips);
		LinkedList<Bike> papa = SortStructure.mergeSortBikesByDurations(listaBikes, topBicicletas);
		return papa;
	}

	public LinkedList<Trip> C4ViajesEstacion(int StationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		LocalDateTime inicio = fechaInicial.minusNanos(1);
		LocalDateTime fin = fechaFinal.plusNanos(1);
		Stack<Trip> list = stackTrips;
		LinkedList<Trip> r = new LinkedList<Trip>();
		ListIterator<Trip> listIte = (ListIterator<Trip>) list.iterator();
		while(listIte.hasNext())
		{
			Trip actual = listIte.next();
			LocalDateTime inicioActual = actual.getStartTime();
			LocalDateTime finActual = actual.getStopTime();
			if(inicioActual.isAfter(inicio)&&finActual.isBefore(fin))
			{
				if(actual.getStartStationId()==StationId)
				{
					actual.setTipo(Tipo.INICIO);
					r.add(actual);
				}
				else if(actual.getEndStationId()==StationId)
				{
					actual.setTipo(Tipo.FIN);
					r.add(actual);
				}
			}
		}
		
		return r;
	}


	public double calcularDistancia(int inicio, int end)
	{
		boolean ini = false;
		boolean fini = false;
		Station referencia = null;
		Station ended = null;
		for(int i = 0; i<listaEncadenadaStations.size()&&(!ini||!fini); i++)
		{
			if(listaEncadenadaStations.get(i).getItem().getStationId()==inicio)
			{
				referencia = listaEncadenadaStations.get(i).getItem();
				ini = !ini;
			}
			if(listaEncadenadaStations.get(i).getItem().getStationId()==end)
			{
				ended = listaEncadenadaStations.get(i).getItem();
				fini = !fini;
			}
		}

		if(referencia != null && ended != null)
		{
			return calculateDistance(ended.getLat(), ended.getLongitude(), referencia.getLongitude(), referencia.getLat());
		}
		else {
			return 0;
		}
	}




	//METODO CREADO POR Maria Jose Ocampo Vargas
	//METODO CREADO POR Maria Jose Ocampo Vargas
	//METODO CREADO POR Maria Jose Ocampo Vargas

	public double calculateDistance (double lat1, double lon1, double longitudReferencia, double latitudReferencia) {

		final int R = 6371*1000; // Radious of the earth in meters 

		double latDistance = Math.toRadians(latitudReferencia-lat1); 

		double lonDistance = Math.toRadians(longitudReferencia-lon1); 

		double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))* Math.cos(Math.toRadians(latitudReferencia)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2); 

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 

		double distance = R * c;

		return distance; 

	}

}
