/**
 * Autor: Mateo David Quintero Reyes
 */
package model.data_structures;

/**
 * Interfaz para una lista generica doblemente encadenada
 * Esta interfaz se hizo basandose en lo visto en clase
 * @param <T> Objeto generico
 */
public interface List<T> extends Iterable<T> {

	//public List();
	
	public void addFirst(T item);
	
	public void removeFirst();
	
	public void add(T item);
	
	public void remove(int pos);
	
	public Node<T> get(int pos);
	
	public int size();
	
	public boolean isEmpty();
	
	

}
