package model.vo;

import java.util.Iterator;

public class Bike implements Comparable<Bike>{

    private int bikeId;
    private int totalTrips;
    private int totalDistance;
    private int totalDuration;

    public Bike(int bikeId, int totalTrips, int totalDistance, int ptotalDuration) {
        this.bikeId = bikeId;
        this.totalTrips = totalTrips;
        this.totalDistance = totalDistance;
        this.totalDuration = ptotalDuration;
    }

    @Override
    public int compareTo(Bike o) {
    	if(this.totalTrips>o.getTotalTrips())
    		return -1;
    	else
    		return 1;
    }
    
    public int compareToByDistances(Bike o) {
    	if(this.totalDistance>o.getTotalDistance())
    		return -1;
    	else
    		return 1;
    }
    
    public int compareToByDurations(Bike o) {
    	if(this.totalDuration>o.getTotalDuration())
    		return -1;
    	else
    		return 1;
    }

    public int getBikeId() {
        return bikeId;
    }

    public int getTotalTrips() {
        return totalTrips;
    }

    public int getTotalDistance() {
        return totalDistance;
    }
    public int getTotalDuration() {
    	return totalDuration;
    }
    
    public void addDistance(double d)
    {
    	totalDistance+=d;
    }
    
    public void addTrips(int t)
    {
    	totalTrips+=t;
    }
    
    public void addDuration(double t)
    {
    	totalDuration+=t;
    }
}
